import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;


public class Poker {
    //number of players
    public static final int NO_PLAYERS = 2;
    public enum Rang { HIGH_CARD, PAIR, TWO_PAIRS, TRIO,
        STRAIGHT, FLUSH, FULL, CARROT, POKER, KING_POKER }

    private Player players[];

    public Poker()
    {
        players = new Player[NO_PLAYERS];
        for (int i = 0; i < NO_PLAYERS; i++) {
            players[i] = new Player(i+1);
        }
    }

    public void LoadPlayersCards(String filename) throws IOException
    {
        System.out.println(String.format("| %4s | %-24s | %-24s | %3s |", "NO", "   Player 1", "   Player 2", "Win"));
        BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
        try
        {
            int r, p1cards = 0, p2cards = 0, no = 1;
            int p1score = 0, p2score = 0;
            while ((r = br.read()) != (-1))
            {
                if(!Character.isWhitespace((char)r))
                {
                    if (p1cards < Player.NO_CARDS)
                        players[0].deck[p1cards++] = new Card(new char[]{(char) r, (char)br.read()});
                    else if(p2cards < Player.NO_CARDS)
                        players[1].deck[p2cards++] = new Card(new char[]{(char) r, (char)br.read()});
                } else if (p1cards == Player.NO_CARDS && p2cards == Player.NO_CARDS) {
                    //System.out.println(String.format("| %8d | %-14s | %-14s | %7s |", no, "nic jesz", "nic jesz", "?"));
                    for (Player p : players) {
                        p.CheckCombinations();
                    }
                    int who_won = WhoWon();
                    if(who_won == NO_PLAYERS)
                        p2score++;
                    else
                        p1score++;
                    System.out.println(String.format("| %4d | %-24s | %-24s | %3s |", no, players[0].toString(),
                            players[1].toString(), ("p" + who_won)));
                    char cha = (char) r;
                    players[0] = new Player(NO_PLAYERS-1);
                    p1cards = 0;
                    players[1] = new Player(NO_PLAYERS);
                    p2cards = 0;
                    no++;
                }
            }
            System.out.println(String.format("Player%d wins:%d\nPlayer%d wins:%d", NO_PLAYERS-1, p1score,
                    NO_PLAYERS, p2score));
        }
        catch (IOException exc)
        {
            throw exc;
        }
    }

    public int WhoWon()
    {
        boolean p1Won = true;
        for (Player.Score s1 : players[0].score)
        {
            p1Won = true;
            for (Player.Score s2 : players[1].score)
            {
                if (s2.type.compareTo(s1.type) > 0)
                    p1Won = false;
                else if (s2.type.equals(s1.type))
                {
                    if (players[1].HighestCard.Value.compareTo(players[0].HighestCard.Value) > 0)
                        p1Won = false;
                }
            }
            if (p1Won)
                return players[0].player_no;
        }
        return players[1].player_no;
    }

    private void Play()
    {

    }
}
