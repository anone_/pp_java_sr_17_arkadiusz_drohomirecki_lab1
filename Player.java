import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ar on 2015-05-05.
 */
public class Player {
    // static constant number of cards
    public final static int NO_CARDS = 5;

    //player number
    public int player_no;
    //player deck
    public Card deck[];
    //player score
    public List<Score> score;
    public Card HighestCard;

    public Player(int i)
    {
        this.player_no = i;
        this.deck = new Card[NO_CARDS];
        this.score = new ArrayList<Score>();
    }

    public class Score
    {
        public Poker.Rang type;
        public List<Card> cards;

        public Score() {
            this.cards = new ArrayList<Card>();
        }

        public String toString()
        {
            String cards = "";
            for (Card c : this.cards)
                cards += " " + c.toString();
            return String.format("%s %s", this.type.toString(), cards);
        }
    }

    public void PrintDeck()
    {
        System.out.println(String.format("Player %d cards: ", player_no));
        for (Card c : deck)
            System.out.println(String.format("%s ", c.toString()));
    }

    public void CheckCombinations()
    {
        this.SameValue();
        if(this.score.size() == 0)
        {
            boolean color = this.Color(), strit = this.Strit();
            if (color && strit)
                this.Poker();
        }

        this.HighCard();
    }

    //Function for checking if pairs exist in players deck
    public void SameValue()
    {
        for (int i = 0; i < Player.NO_CARDS; i++) {
                Score s = new Score();
                s.cards.add(this.deck[i]);
                for (int j = i+1; j < Player.NO_CARDS; j++) {
                    if (this.deck[i].Value == this.deck[j].Value)
                        s.cards.add(this.deck[j]);
                }
                if (s.cards.size() > 1)
                {
                    if (s.cards.size() == 4)
                        s.type = Poker.Rang.CARROT;
                    else if (s.cards.size() == 3)
                        s.type = Poker.Rang.TRIO;
                    else
                        s.type = Poker.Rang.PAIR;

                    boolean contains = false;
                    for (Score sc : this.score)
                        if(sc.type == s.type ||
                                sc.cards.get(0).Value == this.deck[i].Value)
                            contains = true;
                    if (!contains)
                        this.score.add(s);
                }
        }

        //if there is ful or two_pairs in deck
        if (this.score.size() == 2)
        {
            Score s = new Score();
            //currently processed cards need to be copied
            //into new Score object
            for (Score sc : this.score)
                for (Card c : sc.cards) s.cards.add(c);
            //Proper type need to be set
            if (this.score.get(0).type == this.score.get(1).type)
                s.type = Poker.Rang.TWO_PAIRS;
            else
                s.type = Poker.Rang.FULL;

            //score list need to be clear before adding new object
            this.score.clear();
            this.score.add(s);
        }
    }

    //Function for checking colors
    public boolean Color()
    {
        Score s = new Score();
        s.type = Poker.Rang.FLUSH;
        //var to be compared to
        Card.Card_Types c_t = this.deck[0].Type;
        for (Card c : this.deck)
        {
            s.cards.add(c);
            if (c_t != c.Type)
                return false;
        }
        this.score.add(s);
        return true;
    }

    public boolean Strit()
    {
        List<Card> d = new ArrayList<Card>();
        for (Card c : this.deck)
            d.add(c);
        Collections.sort(d, (Card c1, Card c2) -> {
            return c1.Value.compareTo(c2.Value);
        });
        Card.Card_Values last_val = null;
        for (Card c : d)
            if (last_val == null)
                last_val = c.Value;
            else {
                if (((last_val.ordinal() + 1) != c.Value.ordinal()))
                    return false;
                last_val = c.Value;
            }
        Score s = new Score();
        s.type = Poker.Rang.STRAIGHT;
        s.cards = d;
        this.score.add(s);
        return true;
    }

    public void Poker()
    {
        Score s = new Score();
        s.type = Poker.Rang.POKER;
        for (Score sc : this.score)
            if (sc.type == Poker.Rang.STRAIGHT &&
                    sc.cards.get(0).Value.equals(Card.Card_Values.TEN))
                s.type = Poker.Rang.KING_POKER;
        s.cards = this.score.get(0).cards;
        this.score.clear();
        this.score.add(s);
    }

    public void HighCard()
    {
        HighestCard = null;
        for (Card c : this.deck)
        {
            if (HighestCard == null)
                HighestCard = c;
            else
            {
                if (c.Value.compareTo(HighestCard.Value) > 0)
                    HighestCard = c;
            }
        }
        Score s = new Score();
        s.type = Poker.Rang.HIGH_CARD;
        s.cards.add(HighestCard);
        if (this.score.size() == 0)
            this.score.add(s);
    }
    public void PrintScore()
    {
        if (this.score.size() > 0) {
            System.out.println(String.format("Player %d scored:", player_no));
            for (Score s : this.score)
                System.out.println(s.toString());
        }
    }
    public String toString()
    {
        String score = "";
        for (int i = 0; i < this.score.size(); i++) {
            score += this.score.get(i);
            if (i < (this.score.size()-1))
                score += "\n";
        }
        return score;
    }
}
