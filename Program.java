import java.io.IOException;


public class Program {


    public static void main(String[] args) throws IOException {
        // TODO Auto-generated method stub
        Fibonacci f = new Fibonacci();

        f.GetSeries(4000000);
        System.out.println("Fibonacci series:");
        f.DisplaySeries();
        f.GetSum();

        Triangle t = new Triangle();
        t.GetTriangles();
        System.out.println("Triangle numbers:");
        t.DisplayTriangles();
        t.CheckFile("slowa.txt");

        Poker p = new Poker();
        p.LoadPlayersCards("poker.txt");
    }


}