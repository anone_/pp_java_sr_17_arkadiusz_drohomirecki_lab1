public class Card {
    public enum Card_Types { SPADES, HEARTS, DIAMONDS, CLUBS }
    public enum Card_Values { TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE,
        TEN, WALET, QUEEN, KING, AS }
    public Card_Types Type;
    public Card_Values Value;
    public char cards[];

    public Card(char card[])
    {
        this.cards = card;
        this.Value = Char2Value(card[0]);
        this.Type = Char2Type(card[1]);

//        System.out.println(String.format("Card recived: %s %s .", Value.toString(), cards[1]));

    }

    private Card_Values Char2Value(char ch)
    {
        Card_Values v = Card_Values.TWO;
        switch (ch) {
            case '2':
                v = Card_Values.TWO;
                break;
            case '3':
                v = Card_Values.THREE;
                break;
            case '4':
                v = Card_Values.FOUR;
                break;
            case '5':
                v = Card_Values.FIVE;
                break;
            case '6':
                v = Card_Values.SIX;
                break;
            case '7':
                v = Card_Values.SEVEN;
                break;
            case '8':
                v = Card_Values.EIGHT;
                break;
            case '9':
                v = Card_Values.NINE;
                break;
            case 'T':
                v = Card_Values.TEN;
                break;
            case 'J':
                v = Card_Values.WALET;
                break;
            case 'Q':
                v = Card_Values.QUEEN;
                break;
            case 'K':
                v = Card_Values.KING;
                break;
            case 'A':
                v = Card_Values.AS;
                break;
            default:
                break;
        }

        return v;
    }

    private Card_Types Char2Type(char ch)
    {
        Card_Types t = Card_Types.SPADES;
        switch (ch) {
            case 'S':
                t = Card_Types.SPADES;
                break;
            case 'H':
                t = Card_Types.HEARTS;
                break;
            case 'D':
                t = Card_Types.DIAMONDS;
                break;
            case 'C':
                t = Card_Types.CLUBS  ;
                break;
            default:
                break;
        }
        return t;
    }

    public String toString()
    {
        return Character.toString(cards[0]) + Character.toString(cards[1]);
    }
}