import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* Class for handling triangle numbers 
 * and word2num conversion.
 */
public class Triangle
{
    // list of triangle numbers
    public List<Integer> triangles;
    //	number of elements
    public final Integer no_elements = 50;
    // constructor
    public Triangle()
    {
        // triangle list initialization
        triangles = new ArrayList<Integer>();
        // call f for triangle numbers generation
        GetTriangles();
    }

    // triangle numbers generation
    public void GetTriangles()
    {
        // t_n = 0.5 * n * ( n + 1 )
        for (int i = 1; i < no_elements; i++) {
            triangles.add((int)(0.5*i*(i+1)));
        }
    }

    // function for displaying triangle numbers
    public void DisplayTriangles()
    {
        for (Integer i : triangles)
        {
            System.out.print(i + " ");
        }
        System.out.println(" ");
    }

    // function for word2num conversion
    private Integer CountWord(String word)
    {
        Integer sum = 0;
        Integer offset = 96;
        for (char ch : word.toCharArray())
        {
            int temp = (int)ch;
            // 122 and 97 being offset for checking if temp is alpha
            if(temp<=122 & temp>=97)
                // if is add to sum
                sum += temp - offset;
        }

        return sum;
    }

    // checks if word sum is in triangle number
    public Boolean CheckWord(String word)
    {
        word = word.toLowerCase();
        int word_val = CountWord(word);
        for (Integer no : triangles)
        {
            if (no == word_val)
                return true;
        }
        return false;
    }

    // checks all words in file
    public void CheckFile(String filename) throws IOException
    {
        // file being loaded into buffer
        BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
        try
        {
            // r - buffor, now - number of triangle words, words - number of words being reviewed
            int r, now = 0, words = 0;
            String word = "";
            //for test purpose results were printed
            //System.out.println(String.format("| %14s | %11s |", "Word", "Is triangle?"));
            // loads one char after another until the eof
            while((r = br.read()) != -1)
            {
                char ch = (char) r;
                // ignores ' " ' and whitespaces
                if (ch != '\"' && (!Character.isWhitespace(ch)))
                {
                    // comma means that whole word have been completed
                    if (ch != ',')
                    {
                        word += ch;
                    }
                    else
                    {
                        //String answer = "no";
                        //if word count is equal to any triangle number
                        if (CheckWord(word))
                        {
                            //answer = "yes";
                            //number of triangle words must be incremented
                            now++;
                        }
                        //for test purpose results were printed
                        //System.out.println(String.format("| %14s | %11s |", word, answer));
                        word = "";
                        words++;
                    }
                }
            }
            //last word must be handled
            //String answer = "no";
            if (CheckWord(word))
            {
            //    answer = "yes";
                now++;
            }
            //for test purpose results were printed
            //System.out.println(String.format("| %14s | %11s |", word, answer));
            word = ""; words++;
            System.out.println(String.format("== %d/%d words in \'%s\' file are triangle.", now, words, filename));
        }
        catch (IOException exc)
        {
            System.out.println("Problem occurred while file loading.");
        }
    }
}
