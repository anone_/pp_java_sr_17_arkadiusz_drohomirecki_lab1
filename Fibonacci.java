import java.util.ArrayList;
import java.util.List;


public class Fibonacci {
    public List<Integer> series;

    public Fibonacci()
    {
        series = new ArrayList<Integer>();
    }

    public void GetSeries(Integer max)
    {
        int next_nb = 0;
        while (next_nb < max)
        {
            if (series.size() == 0)
            {
                series.add(1);
                series.add(2);
            } else
            {
                next_nb = series.get(series.size() - 1)+series.get(series.size() - 2);
                if (next_nb < max)
                    series.add(next_nb);
            }
        }
    }

    public int GetSum()
    {
        int sum = 0;
        for (Integer i : series)
            if (i%2 == 0)
                sum += i;
        System.out.println("Sum of even numbers in series is equal to: " + sum);
        return sum;
    }

    public void DisplaySeries()
    {
        for (Integer i : series)
        {
            System.out.print(i + " ");
        }
        System.out.println(" ");
    }
}
